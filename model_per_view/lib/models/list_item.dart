class ListItem {
  final String id;
  final String title;
  final String description;

  ListItem({this.id,this.title, this.description});
}