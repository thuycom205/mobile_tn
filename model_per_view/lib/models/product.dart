class Product {
  final String name;
  final int id;
  final String email;
  final String userId;
  final String accessToken;

  Product({this.name,this.id, this.email, this.userId, this.accessToken});

  Product.fromJson(Map<String, dynamic> data)
      : name = data['name'],
        id = data['id'],
        email = data['email'],
        userId = data['userId'],
        accessToken = data['accessToken'];
}
