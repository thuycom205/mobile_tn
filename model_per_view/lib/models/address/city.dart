class City {
  final int countryId;
  final int id;
  final String name;

  City(this.id, this.name, this.countryId);
}