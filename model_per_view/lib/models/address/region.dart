class Region {
  final int id;
  final int cityId;
  final String name;

  Region(this.id, this.cityId, this.name);
}