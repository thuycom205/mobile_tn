class District {
  final int id;
  final int regionId;
  final String name;

  District(this.id, this.regionId, this.name);

}