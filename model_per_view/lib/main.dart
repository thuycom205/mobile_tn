import 'package:flutter/material.dart';
import 'package:model_per_view/ui/views/home_view.dart';
import 'package:model_per_view/ui/views/login_view.dart';
import 'package:model_per_view/ui/views/results_view.dart';
import 'package:model_per_view/ui/views/sign_up_view.dart';
import './service_locator.dart';
import 'package:model_per_view/ui/screens/start_screen.dart';

void main() {
  // Register all the models and services before the app starts
  setupLocator();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Model Per View',
        theme: ThemeData(
            primaryColor: Color.fromARGB(255, 9, 202, 172),
            backgroundColor: Color.fromARGB(255, 26, 27, 30),
            textTheme: Theme.of(context).textTheme.apply(
                fontFamily: 'Open Sans',
                bodyColor: Colors.blueAccent,
                displayColor: Colors.black)),
        home: StartScreen(),
    );
  }
}
