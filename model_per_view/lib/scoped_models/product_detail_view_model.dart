import 'dart:async';
import 'dart:convert';
import 'package:model_per_view/models/product.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:model_per_view/services/web_service.dart';
import 'package:model_per_view/models/list_item.dart';
import 'package:model_per_view/service_locator.dart';
import 'base_model.dart';

import 'package:model_per_view/services/shared_service.dart';
export 'package:model_per_view/enums/view_state.dart';


/// Contains logic for a list view with the general expected functionality.
class ProductDetailModel extends BaseModel {
  bool isLoggedIn =true ;
  String userId;
  String storeId = "default";
  String token;
  String password;
  bool isProcessingAddToCart = false;
  int cartQty;
  WebService _webService = locator<WebService>();

  Product productData;

  String responsePost;

  Future fetchData(int productId) async {
    ShareService shareService = new ShareService();
    shareService.setQty(cartQty);
    this.cartQty = cartQty;
    notifyListeners();
    
    setState(ViewState.Busy);

    productData = await _webService.fetchProductData(productId);

    if (productData == null) {
      setState(ViewState.Error);
    } else {
      setState(productData.id == 0
          ? ViewState.NoDataAvailable
          : ViewState.DataFetched);
    }
  }

  //add item to cart

   Future addProductToCart(int productId) async {
    //send a post request to add item to cart

     isProcessingAddToCart = true;
     notifyListeners();

     Map<String,String> headerMap =  {'Content-Type': 'application/json','Authorization':'Bearer f6ftlqvv51lw8ehxd09k6nntojat1bfe'};

     String payload = "{\"cartItem\": {\"sku\": \"WS12-M-Orange\",\"qty\": 1,\"quote_id\": \"8\"}}";
     if (isLoggedIn) {

          responsePost=  await _webService.addItemToCart(storeId, headerMap, payload);

         isProcessingAddToCart = false;

         notifyListeners();

         String cartResponse = await _webService.getCartInformation(storeId, headerMap);
          final cartInfo = json.decode(cartResponse);

          int cartQty = cartInfo["items_qty"];
          ShareService shareService = new ShareService();
          shareService.setQty(cartQty);
          this.cartQty = cartQty;


          notifyListeners();

         return responsePost;
     }

   }
}