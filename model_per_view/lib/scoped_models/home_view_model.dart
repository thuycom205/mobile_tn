import 'package:model_per_view/models/product.dart';
import 'package:model_per_view/services/shared_service.dart';

import 'base_model.dart';
class HomeModel extends BaseModel {
  int cartQty;

  void refreshData() {
    ShareService shareService = new ShareService();
    this.cartQty = shareService.getCartQty();
    notifyListeners();
  }
  List<Product> _cartList = [];

}