import 'package:model_per_view/models/address/city.dart';
import 'package:model_per_view/models/address/region.dart';
import 'package:model_per_view/models/product.dart';
import 'package:model_per_view/services/shared_service.dart';

import 'base_model.dart';
class COShippingModel extends BaseModel {

  int cartQty;
  City _selectedCity;

  City get selectedCity => _selectedCity;

  set selectedCity(City value) {
    _selectedCity = value;
  }

  List<City> get cityCollection => _cityCollection;

  set cityCollection(List<City> value) {
    _cityCollection = value;
  }

  List<City> _cityCollection = new List<City>();

  Region _selectedRegion;

  Region get selectedRegion => _selectedRegion;

  set selectedRegion(Region value) {
    _selectedRegion = value;
  }

  List<Region> _regionCollection = new List<Region>();


  List<Region> get regionCollection => _regionCollection;

  set regionCollection(List<Region> value) {
    _regionCollection = value;
  }

  void refreshData() {

    _cityCollection.add(City(1, "Hanoi", 1));
    _cityCollection.add(City(2, "HCM", 1));
    notifyListeners();
  }

  void onCitySelected(City city) {
    this._selectedCity = city;
    this._selectedRegion = null;
    this._regionCollection.clear();
    int len = this._regionCollection.length;

    this._regionCollection.add(Region(0, 0, "Chon Quan"));

//    for (int i = 0; i < len -1; i++) {
//      this._regionCollection.removeAt(i);
//
//    }


    if (city.id ==1) {
      this._regionCollection.add(Region(1, 1, "Hai Ba Trung"));
      this._regionCollection.add(Region(1, 2, "Dong Da"));
    }

     else {
      this._regionCollection.add(Region(2, 3, "Quan 1"));
      this._regionCollection.add(Region(2, 4, "Quan Phu Nhuan"));
    }
    notifyListeners();

  }

}