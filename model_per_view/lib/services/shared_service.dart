import 'package:flutter/services.dart';
import 'package:model_per_view/models/address/city.dart';
import 'package:model_per_view/models/address/district.dart';
import 'package:model_per_view/models/address/region.dart';

class ShareService {
  static final ShareService _singleton = new ShareService._internal();

  int cart_qty;

  List<City> cityCollection  = new List<City>();

  List<Region> regionCollection  = new List<Region>();

  List<District> districtCollection  = new List<District>();


  factory ShareService() {

    return _singleton;
  }

  ShareService._internal() {
    this.cart_qty = 1;
    this.loadCSV();

  }
  Future<String> loadAsset(String path) async {
    return await rootBundle.loadString(path);
  }

  void loadCSV() {
    loadAsset('assets/res/city.csv').then((dynamic output) {
      var csvRaw = output;
    });
  }
  void setQty(int qty) {

    this.cart_qty = qty;
  }

  int getCartQty() {
    return this.cart_qty;
  }

}

