import 'package:flutter/material.dart';
import 'package:model_per_view/scoped_models/home_view_model.dart';
import 'package:model_per_view/ui/views/base_view.dart';
import 'package:model_per_view/ui/views/co_shipping.dart';
import 'package:model_per_view/ui/widgets/address_widget.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  BaseView<HomeModel>(
        onModelReady: (model) => model.refreshData(),

        builder: (context, child, model) => Scaffold(

      //  backgroundColor: Theme.of(context).backgroundColor,
        appBar: _buildAppBar(model, context),
        body:
        ListView(
          children: <Widget>[
//            Container(
//                padding: EdgeInsets.only(left: 25.0, right: 25.0),
//                child:
//                Container(
//                  child: Text(model.cartQty.toString()),
//                )
//                ),

            AddressWidget(),
          ],
        ),

        ));
  }
  Widget _buildAppBar(HomeModel model, BuildContext  context) {
    return AppBar(
      title: Text('Home'),
      actions: <Widget>[
        Stack(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (_) {
                      return CheckoutShippingView(

                      );
                    },
                  ),
                );;
              },
            ),
            Container(

              height: 35,
              width: 60,
              padding: EdgeInsets.only(right: 10),
              alignment: Alignment.topRight,
              child: Container(
                width: 20,
                height: 20,
                alignment: Alignment.center,
                padding: EdgeInsets.all(2),
                child: Text("3"),
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
              ),
            )
          ],
        )
      ],
    );
  }
}