import 'package:flutter/material.dart';
import 'package:model_per_view/scoped_models/login_view_model.dart';
import 'package:model_per_view/scoped_models/product_detail_view_model.dart';
import 'package:model_per_view/ui/shared/font_styles.dart';
import 'package:model_per_view/ui/shared/ui_helpers.dart';
import 'package:model_per_view/ui/views/base_view.dart';
import 'package:model_per_view/enums/view_state.dart';

class ProductDetailView extends StatelessWidget {

  final int productId;



  const ProductDetailView({Key key, this.productId}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return  BaseView<ProductDetailModel>(
        onModelReady: (model) => model.fetchData(productId),
        builder: (context, child, model) =>Scaffold(
            backgroundColor: Color.fromARGB(255, 26, 27, 30),
            appBar:_buildAppBar(model,context),
            body: _getBodyUi(context,model)));
  }

  Widget _getAppBarTitle(BuildContext context, ProductDetailModel model) {
    switch (model.state) {
      case ViewState.Busy:
        return Text("");

      case ViewState.DataFetched:
      default:
        return Text(model.productData.name);
    }
  }
  Widget _getBodyUi(BuildContext context, ProductDetailModel model) {
    switch (model.state) {
      case ViewState.Busy:
        return _getLoadingUi(context);

      case ViewState.DataFetched:
      default:
        return _getListUi(context,model);
    }
  }
  Widget _getAddToCartButton(BuildContext context, ProductDetailModel model) {
   if (model.isProcessingAddToCart) {
     return   RaisedButton(
       
       child: Text("Adding"),
       onPressed: (){
         model.addProductToCart(productId);
       },
     );
   } else {
     return RaisedButton(
       child: Text("Add to cart"),
       onPressed: (){
         model.addProductToCart(productId);
       },
     );
   }


  }
  Widget _getLoadingUi(BuildContext context) {
    return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            CircularProgressIndicator(
                valueColor:
                AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor)),
            Text('Fetching data ...')
          ],
        ));
  }

  Widget _getListUi(BuildContext context, ProductDetailModel model) {

    return (model.productData != null)?
    Container(
      padding: EdgeInsets.all(15.0),
      margin: EdgeInsets.only(top: 50.0),
      child: ListView(children: <Widget>[

        Text(model.productData.name, style: viewTitle),
        _getAddToCartButton(context, model)

      ]),
    ): _getLoadingUi(context);
  }

  Widget _buildAppBar(ProductDetailModel model, BuildContext  context) {
    return AppBar(
      title: Text('Home'),
      actions: <Widget>[
        Stack(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.pushNamed(context, '/cart');
              },
            ),
            Container(
              height: 35,
              width: 60,
              padding: EdgeInsets.only(right: 10),
              alignment: Alignment.topRight,
              child: Container(
                width: 20,
                height: 20,
                alignment: Alignment.center,
                padding: EdgeInsets.all(2),
                child: Text(model.cartQty.toString()),
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
              ),
            )
          ],
        )
      ],
    );
  }

}

