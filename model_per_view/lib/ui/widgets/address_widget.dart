import 'package:flutter/material.dart';
import 'package:model_per_view/ui/shared/font_styles.dart';
import 'package:model_per_view/ui/shared/ui_helpers.dart';
/// A modal overlay that will show over your child widget (fullscreen) when the show value is true
///
/// Wrap your scaffold in this widget and set show value to model.isBusy to show a loading modal when
/// your model state is Busy
class AddressWidget extends StatelessWidget {
  final String title;
  final bool show;

  const AddressWidget({
    this.title = 'Please wait...',
    this.show = false});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;


    return Material(
        child: Container(
          padding: EdgeInsets.all(5.0),
          margin: EdgeInsets.only(top: 1.0),
          child: Container(
              decoration: BoxDecoration(
                  borderRadius:  BorderRadius.all(
                    Radius.circular(5.0)
                ),
//                border: Border.all(
//                    width: 3.0,
//                ),
                 border: Border(
                   bottom: BorderSide(width: 1.0, color: Colors.grey),
                   left: BorderSide(width: 1.0, color: Colors.grey),
                   right: BorderSide(width: 1.0, color: Colors.grey),
                   top: BorderSide(width: 1.0, color: Colors.grey),
                ),
              ),
            child: Row(
              children: <Widget>[
                Container(

                  padding: EdgeInsets.all(8.0),
                  decoration: BoxDecoration(

//                    border: Border(
//                      right: BorderSide(width: 2.0, color: Colors.grey),
//                     // bottom: BorderSide(width: 16.0, color: Colors.lightBlue.shade900),
//                    ),
                  ),
                  child: Column(
                      children: <Widget>[
                      Text("SỐ ĐIỆN THOẠI"),
                      Row(
                        children: <Widget>[
                          Icon(Icons.flag,size: 25,),
                          Text('+84')
                        ],
                      ) ,

                    ],
                  ),
                ),
                Container(
                  width: 0.5,
                    height: 35,
                    decoration: BoxDecoration(

                      border: Border(
                        right: BorderSide(width: 2.0, color: Colors.grey),
                        // bottom: BorderSide(width: 16.0, color: Colors.lightBlue.shade900),
                      ),
                    )
                ),

                Container(
                    constraints:  new BoxConstraints(minWidth: 100, maxWidth: 290),

                   // width: 300,
                  height: 45,
                    margin: EdgeInsets.only(top: 30),
                    padding: EdgeInsets.all(8.0),
                    child: TextField(
                      decoration: InputDecoration.collapsed(
                          hintText: "phone number",
                          hintStyle:
                          TextStyle(color: Colors.grey[600], fontSize: 12.0))

                ))

              ],
            ),
          ),
        )
    );
  }
}
