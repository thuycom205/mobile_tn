import 'package:flutter/material.dart';
import 'package:model_per_view/ui/views/home_view.dart';
import 'package:model_per_view/ui/views/login_view.dart';
import 'package:model_per_view/ui/views/results_view.dart';
import 'package:model_per_view/ui/views/sign_up_view.dart';

/// START SCREEN
/// This view holds all tabs & its models: home, vehicles, upcoming & latest launches, & company tabs.
class StartScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  int _currentIndex = 0;
  static final List<Widget>  _tabs = [
    HomeView(),LoginView(),SignUpView(),ResultsView(),ResultsView()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _tabs[_currentIndex],
      bottomNavigationBar:BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: (index) => setState(() => _currentIndex = index),
        currentIndex: _currentIndex,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            title: Text("Home"),
            icon: const Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            title: Text("Shop"
            ),
            icon: const Icon(Icons.shop),


          ),
          BottomNavigationBarItem(
            title: Text("Search"
            ),
            icon: const Icon(Icons.search),


          ), BottomNavigationBarItem(
            title: Text("Notification"
            ),
            icon: const Icon(Icons.notifications),


          ), BottomNavigationBarItem(
            title: Text("Profile"
            ),
            icon: const Icon(Icons.verified_user),


          ),

        ],
      ),

    );
  }

}